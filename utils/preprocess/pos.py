import argparse
import json
import os


UNIVERSAL_POS_TAGS_FILE_MAPPING = {
  "spanish_cast3lb" : "es-cast3lb.map",
  "german_tiger": "de-tiger.map",
  "dutch_alpino": "nl-alpino.map",
  "danish_ddt": "da-ddt.map",
  "portuguese_bosque": "pt-bosque.map",
  "slovene_sdt": "sl-sdt.map",
  "italian_isst": "it-isst.map",
  "greek_gdt": "el-gdt.map",
  "swedish_talbanken05": "sv-talbanken.map"}

def read_map(file_path):
  pos_map = {}
  with open(file_path) as fin:
    for line in fin:
      items = line.strip().split('\t')
      pos_map[items[0]] = items[1]
  return pos_map

def read(file_path, tag_map_path, dataset, pos_index):
  tokens, labels = [], []
  examples = []
  if tag_map_path is not None:
    pos_map = read_map(os.path.join(tag_map_path, UNIVERSAL_POS_TAGS_FILE_MAPPING[dataset]))
  with open(file_path) as fin:
    for line in fin:
      if line.startswith("#"):
        continue
      if line == "\n":
        examples.append({
          "tokens": tokens,
          "labels": labels})
        tokens, labels = [], []
      else:
        items = line.strip().split('\t')
        token, pos = items[1], items[pos_index]
        if "_" in pos and pos != "_":
          token = token.split("_")
          pos = pos.split("_")
          assert(len(pos) == len(token))
          tokens.extend(token)
          if tag_map_path is not None:
            labels.extend([pos_map[p] for p in pos])
          else:
            labels.extend(pos)
        else:
          tokens.append(token)
          if tag_map_path is not None:
            labels.append(pos_map[pos])
          else:
            labels.append(pos)
  return examples

def dump(examples, file_path):
  with open(file_path, 'w') as fout:
    for example in examples:
      fout.write(json.dumps(example) + "\n")
  fout.close()
  
if __name__ == "__main__":
  parser = argparse.ArgumentParser()
  parser.add_argument("--input_path", help="The input directory", type=str)
  parser.add_argument("--output_path", help="The output directory", type=str)
  parser.add_argument("--tag_map_path", help="The directory to universal-pos-tags", type=str, default=None)
  parser.add_argument("--data_source", type=str, choices=["conll", "ud"], default="conll")
  args = parser.parse_args()

  file_name = os.path.basename(args.input_path)
  print("Processing: ", file_name)

  output_dir = os.path.dirname(args.output_path)
  if not os.path.exists(output_dir):
    os.makedirs(output_dir)

  print("Reading from {}".format(args.input_path))
  if args.data_source == "conll":
    dataset = None
    for key in UNIVERSAL_POS_TAGS_FILE_MAPPING:
      if  file_name.startswith(key):
        dataset = key
    if dataset is None:
      raise ValueError("{} is not supported.".format(file_name))
    examples = read(args.input_path, args.tag_map_path, dataset, pos_index=4)
  elif args.data_source == "ud":
    examples = read(args.input_path, args.tag_map_path, None, pos_index=3)

  print("Writing to {}".format(args.output_path))
  dump(examples, args.output_path)