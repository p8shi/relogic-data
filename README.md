Terra

This project is to host datasets for [relogic](https://github.com/Impavidity/relogic).

Datasets

- Part of Speech Tag
  - CoNLL2006/CoNLL2007
  - Universal Dependency v1.4
- Dependency Parsing

- Named Entity Recognition



How to use

- Setup

```
bash scripts/setup.sh
```

- POS tag

Assume you have the dataset `conll2006_ten_lang` and `conll_2007_gre_hung_ita` downloaded in `data_path`.
Then you can run 

```
bash scripts/conll_pos.sh
```
and just follow the instruction.

The language codes can be `es`, `el`, `de`, `da`, `el`, `it`, `pt` and `sl`.