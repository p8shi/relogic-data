read -e -p "dataset path: " dataset_path
read -p "language: " language

if [ ${language} == "es" ]; then
  train_file_path=conll2006_ten_lang/data/spanish/cast3lb/train/spanish_cast3lb_train.conll
  test_file_path=conll2006_ten_lang/data/spanish/cast3lb/test/spanish_cast3lb_test_gs.conll
  output_path=datasets/pos/conll2006
fi

if [ ${language} == "nl" ]; then
  train_file_path=conll2006_ten_lang/data/dutch/alpino/train/dutch_alpino_train.conll
  test_file_path=conll2006_ten_lang/data/dutch/alpino/test/dutch_alpino_test_gs.conll
  output_path=datasets/pos/conll2006
fi

if [ ${language} == "de" ]; then
  train_file_path=conll2006_ten_lang/data/german/tiger/train/german_tiger_train.conll
  test_file_path=conll2006_ten_lang/data/german/tiger/test/german_tiger_test_gs.conll
  output_path=datasets/pos/conll2006
fi

if [ ${language} == "da" ]; then
  train_file_path=conll2006_ten_lang/data/danish/ddt/train/danish_ddt_train.conll
  test_file_path=conll2006_ten_lang/data/danish/ddt/test/danish_ddt_gs.conll
  output_path=datasets/pos/conll2006
fi

if [ ${language} == "el" ]; then
  train_file_path=conll_2007_gre_hung_ita/data/greek/gdt/train/greek_gdt_train.conll
  test_file_path=conll_2007_gre_hung_ita/data/greek/gdt/test/greek_gdt_test.conll
  output_path=datasets/pos/conll2007
fi

if [ ${language} == "it" ]; then
  train_file_path=conll_2007_gre_hung_ita/data/italian/isst/train/italian_isst_train.conll
  test_file_path=conll_2007_gre_hung_ita/data/italian/isst/test/italian_isst_test.conll
  output_path=datasets/pos/conll2007
fi

if [ ${language} == "pt" ]; then
  train_file_path=conll2006_ten_lang/data/portuguese/bosque/treebank/portuguese_bosque_train.conll
  test_file_path=conll2006_ten_lang/data/portuguese/bosque/test/portuguese_bosque_test_gs.conll
  output_path=datasets/pos/conll2006
fi

if [ ${language} == "sl" ]; then
  train_file_path=conll2006_ten_lang/data/slovene/sdt/treebank/slovene_sdt_train.conll
  test_file_path=conll2006_ten_lang/data/slovene/sdt/test/slovene_sdt_test_gs.conll
  output_path=datasets/pos/conll2006
fi

if [ ${language} == "sv" ]; then
  train_file_path=conll2006_ten_lang/data/swedish/talbanken05/train/swedish_talbanken05_train.conll
  test_file_path=conll2006_ten_lang/data/swedish/talbanken05/test/swedish_talbanken05_test_gs.conll
  output_path=datasets/pos/conll2006
fi

python utils/preprocess/pos.py \
  --input_path ${dataset_path}/${train_file_path} \
  --output_path ${output_path}/${language}/train.json \
  --tag_map_path dependency/universal-pos-tags
python utils/preprocess/pos.py \
  --input_path ${dataset_path}/${test_file_path} \
  --output_path ${output_path}/${language}/test.json \
  --tag_map_path dependency/universal-pos-tags