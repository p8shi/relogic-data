read -e -p "dataset path: " dataset_path
read -p "language: " language

if [ ${language} == "bg" ]; then
  train_file_path=UD_Bulgarian/bg-ud-train.conllu
  dev_file_path=UD_Bulgarian/bg-ud-dev.conllu
  test_file_path=UD_Bulgarian/bg-ud-test.conllu
  output_path=datasets/pos/ud-v1.4
fi

if [ ${language} == "nl" ]; then
  train_file_path=UD_Dutch/nl-ud-train.conllu
  dev_file_path=UD_Dutch/nl-ud-dev.conllu
  test_file_path=UD_Dutch/nl-ud-test.conllu
  output_path=datasets/pos/ud-v1.4
fi

if [ ${language} == "de" ]; then
  train_file_path=UD_German/de-ud-train.conllu
  dev_file_path=UD_German/de-ud-dev.conllu
  test_file_path=UD_German/de-ud-test.conllu
  output_path=datasets/pos/ud-v1.4
fi

if [ ${language} == "da" ]; then
  train_file_path=UD_Danish/da-ud-train.conllu
  dev_file_path=UD_Danish/da-ud-dev.conllu
  test_file_path=UD_Danish/da-ud-test.conllu
  output_path=datasets/pos/ud-v1.4
fi

if [ ${language} == "en" ]; then
  train_file_path=UD_English/en-ud-train.conllu
  dev_file_path=UD_English/en-ud-dev.conllu
  test_file_path=UD_English/en-ud-test.conllu
  output_path=datasets/pos/ud-v1.4
fi

if [ ${language} == "es" ]; then
  train_file_path=UD_Spanish/es-ud-train.conllu
  dev_file_path=UD_Spanish/es-ud-dev.conllu
  test_file_path=UD_Spanish/es-ud-test.conllu
  output_path=datasets/pos/ud-v1.4
fi

if [ ${language} == "fa" ]; then
  train_file_path=UD_Persian/fa-ud-train.conllu
  dev_file_path=UD_Persian/fa-ud-dev.conllu
  test_file_path=UD_Persian/fa-ud-test.conllu
  output_path=datasets/pos/ud-v1.4
fi

if [ ${language} == "hu" ]; then
  train_file_path=UD_Hungarian/hu-ud-train.conllu
  dev_file_path=UD_Hungarian/hu-ud-dev.conllu
  test_file_path=UD_Hungarian/hu-ud-test.conllu
  output_path=datasets/pos/ud-v1.4
fi

if [ ${language} == "pl" ]; then
  train_file_path=UD_Polish/pl-ud-train.conllu
  dev_file_path=UD_Polish/pl-ud-dev.conllu
  test_file_path=UD_Polish/pl-ud-test.conllu
  output_path=datasets/pos/ud-v1.4
fi

if [ ${language} == "it" ]; then
  train_file_path=UD_Italian/it-ud-train.conllu
  dev_file_path=UD_Italian/it-ud-dev.conllu
  test_file_path=UD_Italian/it-ud-test.conllu
  output_path=datasets/pos/ud-v1.4
fi

if [ ${language} == "pt" ]; then
  train_file_path=UD_Portuguese/pt-ud-train.conllu
  dev_file_path=UD_Portuguese/pt-ud-dev.conllu
  test_file_path=UD_Portuguese/pt-ud-test.conllu
  output_path=datasets/pos/ud-v1.4
fi

if [ ${language} == "ro" ]; then
  train_file_path=UD_Romanian/ro-ud-train.conllu
  dev_file_path=UD_Romanian/ro-ud-dev.conllu
  test_file_path=UD_Romanian/ro-ud-test.conllu
  output_path=datasets/pos/ud-v1.4
fi

if [ ${language} == "sk" ]; then
  train_file_path=UD_Slovak/sk-ud-train.conllu
  dev_file_path=UD_Slovak/sk-ud-dev.conllu
  test_file_path=UD_Slovak/sk-ud-test.conllu
  output_path=datasets/pos/ud-v1.4
fi

if [ ${language} == "sl" ]; then
  train_file_path=UD_Slovenian/sl-ud-train.conllu
  dev_file_path=UD_Slovenian/sl-ud-dev.conllu
  test_file_path=UD_Slovenian/sl-ud-test.conllu
  output_path=datasets/pos/ud-v1.4
fi

if [ ${language} == "sv" ]; then
  train_file_path=UD_Swedish/sv-ud-train.conllu
  dev_file_path=UD_Swedish/sv-ud-dev.conllu
  test_file_path=UD_Swedish/sv-ud-test.conllu
  output_path=datasets/pos/ud-v1.4
fi

python utils/preprocess/pos.py \
  --input_path ${dataset_path}/${train_file_path} \
  --output_path ${output_path}/${language}/train.json \
  --data_source ud
python utils/preprocess/pos.py \
  --input_path ${dataset_path}/${dev_file_path} \
  --output_path ${output_path}/${language}/dev.json \
  --data_source ud
python utils/preprocess/pos.py \
  --input_path ${dataset_path}/${test_file_path} \
  --output_path ${output_path}/${language}/test.json \
  --data_source ud